
import View.TableView;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.HighScore;
import model.PokerModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */

public class MVCPoker extends Application{
    private int windowHeight=720;
    private int windowWidth=1080;
    private HighScore highScore;
    
    /**
     * start the application
     * @param primaryStage
     * @throws java.io.IOException
     * @throws java.io.EOFException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    @Override
    public void start(Stage primaryStage) throws IOException, EOFException, FileNotFoundException, ClassNotFoundException  {
        highScore = new HighScore();
        deserialize();
        PokerModel model = new PokerModel(highScore);
        TableView view = new TableView(model, windowWidth, windowHeight);
      
        model.addObserver(view);
        Scene scene = new Scene(view, windowWidth, windowHeight);
        
        primaryStage.setResizable(false);
        primaryStage.setTitle("Texas Hold'em");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    
    public void deserialize() throws IOException, ClassNotFoundException, EOFException{
        try
        {
         FileInputStream fileIn = new FileInputStream("highScore3.ser");
         ObjectInputStream in = new ObjectInputStream(fileIn);
         highScore = (HighScore) in.readObject();
         in.close();
         fileIn.close();
        }
        catch(FileNotFoundException e){
            System.out.println("hitta inte filen men skapar ny sen");
        }
    }

}


