/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javafx.application.Platform;
import model.PokerModel;
import View.TableView;
import java.io.IOException;

/**
 *
 *  @author Stefan Vasic And Manjinder Singh
 */
public class PokerController {
    
    private PokerModel model;
    private TableView tableView;
    
    /**
     * create a new poker controller
     * @param tableView
     * @param model 
     */
    public PokerController(TableView tableView, PokerModel model){
        this.tableView = tableView;
        this.model = model;
    }
    
    /**
     * handle chip 5 button event
     */
    public void handleChip5ButtonEvent(){
        System.out.println("5");
        model.raiseBet(5);
    }
    /**
     * handle chip 5 button event
     */
    public void handleChip10ButtonEvent(){
        model.raiseBet(10);
        System.out.println("10");
    }
    /**
     * handle chip 25 button event
     */
    public void handleChip25ButtonEvent(){
        model.raiseBet(25);
        System.out.println("25");
    }
    /**
     * handle bet button event
     */
    public void handleBetButtonEvent(){
        if("Allin".equals(model.getPlayers().get(0).getState()) || "Allin".equals(model.getPlayers().get(1).getState())){
            
        }
        else{
            System.out.println("bet");
            model.bet();
        }
    }
    /**
     * handle check button event
     */
    public void handleCheckButtonEvent(){
        if("Allin".equals(model.getPlayers().get(0).getState()) || "Allin".equals(model.getPlayers().get(1).getState()) || "Bet".equals(model.getPlayers().get(0).getState()) || "Bet".equals(model.getPlayers().get(1).getState())){
            
        }
        else{
            model.check();
            System.out.println("check");
        }
    }
    /**
     * handle fold button event
     */
    public void handleFoldButtonEvent(){
        model.fold();
        System.out.println("fold");
    }
    /**
     * handle all in event
     */
    public void handleAllInEvent(){
        model.allIn();

        System.out.println("all in");
    }
    /**
     * handle clear bet event
     */
    public void handleClearBetEvent(){
        model.clearBet();
        System.out.println("clear");
    }
    /**
     * handle exit event
     */
    public void handleExitEvent() throws IOException{
        model.serialize();
        Platform.exit();
    }
    /**
     * handle reset game event
     */
    public void handleResetGame(){
        model.dealCardsToPlayers();
    }
    /**
     * handle flip button 1 event
     */
    public void handleFlipButton1Event(){
        tableView.flipPlayer1Cards();
    }
    /**
     * handle flip button 2 event
     */
    public void handleFLipButton2Event(){
        tableView.flipPlayer2Cards();
    }
    
    /**
     * handle load button event
     */
    public void handleLoadButtons(){
         
        if("AI".equals(model.getPlayers().get(1).getName())){
            tableView.loadButtons(Boolean.FALSE);
        }
        else{
            tableView.loadButtons(Boolean.TRUE);
        }
        tableView.addEventHandlers(this);
    }
    
    public void handleHighscore(){
        tableView.highscorePopUp();
    }
    
    /**
     * handle nr of players
     * @param name1
     * @param name2 if empty = AI
     */
    public void handleNrOfPlayers(String name1, String name2){
        model.getPlayers().clear();
        if(!name2.isEmpty()){
            model.addPlayer(name1);
            model.addPlayer(name2);
        }
        else{
            model.addPlayer(name1);
            model.addAI();
        }
        model.setNrOfRounds(0);
        if(!model.getIsPlayer1())
            model.setPlayersTurn();
        model.dealCardsToPlayers();
        model.removeCardsOnTable();
        model.clearPotSize();
        System.out.println(model.getAllPlayersName());
    }
    
    /**
     * handle start game event
     */
    public void handleStartGame(){
        tableView.startGamePopUp();
    }
}
