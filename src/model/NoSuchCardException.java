package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author stefanvasic
 */
public class NoSuchCardException extends RuntimeException{
    
    public NoSuchCardException(){
        
    }
    
    public NoSuchCardException(String message) {
        super(message);
    }
}
