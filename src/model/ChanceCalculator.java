/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class ChanceCalculator {
    private double chance;
    private HandRanking handRanking;
    
    /**
     * create a new chance calculator
     */
    public ChanceCalculator(){
        chance = 0.0;
        handRanking=new HandRanking();
    }
    /**
     * calculate chance of hand
     * @param ai
     * @return chance
     */
    public double calculateChanceOfHand(AI ai){
        chance=0.0;
        //before the flop
        if(ai.getHand().getHand().size()==2){
            chance += ai.getHand().getCard(0).getRank().ordinal();
            System.out.println(ai.getHand().getCard(0).getRank().ordinal());
            chance += ai.getHand().getCard(1).getRank().ordinal();
            System.out.println(ai.getHand().getCard(1).getRank().ordinal());
            if(ai.getHand().getCard(0).getRank().equals(ai.getHand().getCard(1).getRank())){
                chance+=chance += ai.getHand().getCard(0).getRank().ordinal()*1.5;
            }
        
            if(ai.getHand().getCard(0).getSuit().equals(ai.getHand().getCard(1).getSuit())){
                chance+=5;
            }
        }
        
       
        //after the flop
        if(ai.getHand().getHand().size()>2){
            if(handRanking.checkRoyalFlush(ai.getHand())){
                chance=99.0;
            }
            else if(handRanking.checkStraightFlush(ai.getHand())){
                chance=90.0;
            }
            else if(handRanking.checkFourOfAKind(ai.getHand())){
            // 60 + the value of a card that is in the four of a kind
                chance = 60 + ai.getHand().getCard(3).getRank().ordinal(); 
             }
             else if(handRanking.checkFullHouse(ai.getHand())){
                 chance=70;
            }
             else if(handRanking.checkFlush(ai.getHand())){
                 chance = 65;
             }
             else if(handRanking.checkStraight(ai.getHand())){
                 chance=60;
             }
             else if(handRanking.checkThreeOfAKind(ai.getHand())){
                 chance=55;
             }
             else if(handRanking.checkTwoPairs(ai.getHand())){
                 chance=50;
             }
             else if(handRanking.checkPair(ai.getHand())){
                 chance=45;
             }
             else
                 chance-=20;
        }
        
        return chance;
    
    }
}
