package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
/**
 * Objects of this class represents a deck of cards. 
 */
import java.util.ArrayList;
import java.util.Collections;
public class Deck {
    //referens
    ArrayList<Card> deck;
    
    //Constructor
    public Deck(){
        deck = new ArrayList<>();
        fill();
    }
    
    /**
     * @return numer of cards in the deck
     */
    public int getNoOfCards(){
        int noOfCards = deck.size();
        return noOfCards;
    }
    
    /**
     * @return the card that was removed
     * removes a card and returns it
     */
    public Card dealCard()throws NoSuchCardException{
        Card remove=null;
        if(deck.isEmpty()){
            throw new NoSuchCardException("Deck is empty");
            
        }
        else{
            remove = deck.remove(0);
        }
        return remove;
    }
    
    /**
     * shuffles the deck of cards
     */
    public void shuffleCards(){
        Collections.shuffle(deck);
    }
    
    /**
     * shuffles the deck of cards
     */
    public void fill(){
        deck.clear();
        for(Card.Suit suit: Card.Suit.values()){
            for(Card.Rank rank: Card.Rank.values()){
                Card card = new Card(rank, suit);
                    deck.add(card);
            }
        }       
    }
    
    /**
     * @return a string with all cards left in the deck
     */
    @Override
    public String toString(){
        String info="";
        for (Card deck1 : deck) {
            info += deck1 + " \n";
        }
        return info;
    }
}
