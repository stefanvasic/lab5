/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class PokerModel extends Observable{
    private Deck pokerDeck;
    private ArrayList<Player> players;
    private int potSize;
    private boolean isPlayer1;
    private int nrOfRounds;
    private ArrayList<Card> table;
    private HighScore highScore;
    
    /**
     * create a new poker model
     * this is the main game logic
     * @param highScore
     * @throws java.io.IOException
     * @throws java.io.EOFException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    public PokerModel(HighScore highScore) throws IOException, EOFException, FileNotFoundException, ClassNotFoundException{
        pokerDeck=new Deck();
        players = new ArrayList();
        table = new ArrayList();
        potSize=0;
        isPlayer1=true;
        nrOfRounds=0;
        this.highScore=highScore;
        
    }
    /**
     * 
     * @return arraylist players
     */
    public ArrayList<Player> getPlayers(){
        return players;
    }
    /**
     * 
     * @return nrOfRounds
     */
    public int getNrOfRounds(){
        return nrOfRounds;
    }
    /**
     * clear pot size on table
     */
    public void clearPotSize(){
        potSize=0;
        setChanged();
        notifyObservers("playerChips");
    }
    /**
     * 
     * @return true if its player 1's turn
     */
    public boolean getIsPlayer1(){
        return isPlayer1;
    }
    /**
     * set nr of rounds
     * @param rounds 
     */
    public void setNrOfRounds(int rounds){
        if(nrOfRounds!=rounds){
            nrOfRounds=rounds;
        }
        setChanged();
        notifyObservers(rounds);
    }
    /**
     * deal 2 cards to each player and notify view
     */
    public void dealCardsToPlayers(){
        players.get(0).getHand().getHand().clear();
        players.get(1).getHand().getHand().clear();
        pokerDeck.fill();
        pokerDeck.shuffleCards();
        for (Player player : players) {
            player.getHand().addCard(pokerDeck.dealCard());
            player.getHand().addCard(pokerDeck.dealCard());
        }
        setChanged();
        notifyObservers("dealCardsToPlayers");
        
    }
    /**
     * 
     * @return potSize
     */
    public int getPotSize(){
        return potSize;
    }
    /**
     * add a new human player
     * @param name 
     */
    public void addPlayer(String name){
        Player tmp = new HumanPlayer(name);
        players.add(tmp);
        setChanged();
        notifyObservers("playerNames");
    }
    /**
     * add a new AI
     */
    public void addAI(){
        Player tmp = new AI();
        players.add(tmp);
        setChanged();
        notifyObservers("playerNames");
        notifyObservers("ai");
        
    }
    /**
     * handle check event
     */
    public void check(){
        nextRound();
        if(players.get(1) instanceof AI){
            AI tmp = (AI) players.get(1);
            tmp.setChanceOfWinning();
            if(tmp.getChanceOfWinning()<20){
                players.get(1).setState("Check");
                setChanged();
                notifyObservers("AIaction");
                nextRound();
            }
            else{
                tmp.setState("Bet");
                if(tmp.getChips()>25)
                    raiseBet(25);
                else
                    raiseBet(tmp.getChips());
                bet();
            }
        }
    }
    /**
     * handle fold event
     */
    public void fold(){
        if(isPlayer1){
            players.get(1).setChips(potSize);
            System.out.println("Player2 won");
            nrOfRounds=0;
        }
        else{
            players.get(0).setChips(potSize);
            System.out.println("Player1 won");
            nrOfRounds=0;
            setPlayersTurn();
        }
        clearPotSize();

        setChanged();
        notifyObservers("playerChips");
        removeCardsOnTable();
        dealCardsToPlayers();
        players.get(0).setState("");
        players.get(1).setState("");
        players.get(0).clearLastBet();
        players.get(1).clearLastBet();
    }
    /**
     * handle all in event
     */
    public void allIn(){
        int tmp;
        if(isPlayer1){
            players.get(0).setState("Allin");
            tmp=players.get(0).getChips();
        }
        else{
            if(!"Allin".equals(players.get(0).getState()))
                nrOfRounds--;
            players.get(1).setState("Allin");
            tmp=players.get(1).getChips();
        }
        raiseBet(tmp);
        bet();
    }
    /**
     * deal a new card to the table
     */
    public void cardToTable(){
        Card tmp = pokerDeck.dealCard();
        for (Player player : players) {
            player.getHand().addCard(tmp);
        }
        table.add(tmp);
    }
    /**
     * remove the cards on the table
     */
    public void removeCardsOnTable(){
        table.clear();
        setChanged();
        notifyObservers("removeCardsOnTable");
    }
    /**
     * 
     * @return playername
     */
    public String getPlayerName(){
        if(isPlayer1)
            return players.get(0).getName();
        else
            return players.get(1).getName();
    }
    /**
     * 
     * @return table
     */
    public ArrayList<Card> getTable(){
        return table;
    }
    /**
     * 
     * @return all the players names
     */
    public String getAllPlayersName(){
        String info = players.get(0).getName();  
        info+= "\n" + players.get(1).getName();  
        return info;
    }
    
    /**
     * 
     * @return the cards on the table, string
     */
    public String cardsOnTable(){
        String info = " ";
        for (Card table1 : table) {
            info += table1.toString() + " ,";
        }
        return info;
    }
    /**
     * deal the flop to the table and notify view
     */
    public void dealFlop(){
        for(int i=0;i<3;i++)
        {
            cardToTable();
        }
        setChanged();
        notifyObservers("showcard");
    }
    /**
     * deal the turn to the table and notify the view
     */
    public void dealTurn(){
        cardToTable();
        setChanged();
        notifyObservers("showcard");
    }
    /**
     * deal the river to the table and notify view
     */
    public void dealRiver(){
        cardToTable();
        setChanged();
        notifyObservers("showcard");
    }
    /**
     * sets the players turn
     */
    public void setPlayersTurn(){
        isPlayer1 = !isPlayer1;
        setChanged();
        notifyObservers("updateTurn");
    }
    /**
     * 
     * @return true if it's player 1's turn
     */
    public String getPlayersTurn(){
        if(isPlayer1)
            return "Player 1";
        else
            return "Player 2";
    }
    /**
     * raise bet
     * @param bet 
     */
    public void raiseBet(int bet){
        if(isPlayer1)
            players.get(0).setBetChips(bet);
        else
            players.get(1).setBetChips(bet);
        System.out.println("bet:" + bet);
        setChanged();
        notifyObservers("playerChips");
    }
    /**
     * handle bet event
     */
    public void bet(){
        if(isPlayer1){
            if(!"Bet".equals(players.get(1).getState())){
                potSize+=players.get(0).getBetChips();
                players.get(0).resetBetChips();
                if(!"Allin".equals(players.get(0).getState()))
                    players.get(0).setState("Bet");
                if(players.get(1) instanceof AI){
                    setChanged();
                    notifyObservers("playerChips");
                    nextRound();
                }
            }
            else if("Bet".equals(players.get(1).getState())){
                if(players.get(0).getLastBet()==players.get(1).getLastBet()){
                    potSize+=players.get(0).getBetChips();
                    players.get(0).resetBetChips();
                    if(!"Allin".equals(players.get(0).getState()))
                        players.get(0).setState("Bet");
                }
            }
            if(players.get(1) instanceof AI && !"Allin".equals(players.get(0).getState()) && !"Bet".equals(players.get(1).getState())){
                AI tmp = (AI) players.get(1);
                tmp.setChanceOfWinning();
                System.out.println(tmp.getChanceOfWinning());
                if(tmp.getChanceOfWinning()>=8){
                    tmp.setBetChips(players.get(0).getLastBet());
                    players.get(0).clearLastBet();
                    potSize+=players.get(1).getBetChips();
                    players.get(1).resetBetChips();
                    players.get(1).setState("Bet");
                    setChanged();
                    notifyObservers("AIaction");
                }
                if(tmp.getChanceOfWinning()<8){
                    fold();
                    setPlayersTurn();
                    players.get(1).setState("Fold");
                    setChanged();
                    notifyObservers("AIaction");
                }      
            }
            else if(players.get(1) instanceof AI && "Allin".equals(players.get(0).getState())){
                AI tmpAI =(AI) players.get(1);
                tmpAI.setChanceOfWinning();
                if(tmpAI.getChanceOfWinning()<=50){
                    System.out.println("AI fold");
                    fold();
                    setPlayersTurn();
                    players.get(1).setState("Fold");
                    setChanged();
                    notifyObservers("AIaction");
                }
                else if(tmpAI.getChanceOfWinning()>50){
                    System.out.println("AI allin");
                    raiseBet(players.get(1).getChips());
                    bet();
                    players.get(1).setState("Allin");
                    setChanged();
                    notifyObservers("AIaction");
                } 
            }
            setChanged();
            notifyObservers("playerChips");
            nextRound();
        }
        else{
            if(!"Bet".equals(players.get(0).getState()))
                nrOfRounds--;
            if(players.get(1).getBetChips()==players.get(0).getLastBet() || "Allin".equals(players.get(0).getState())){
                potSize+=players.get(1).getBetChips();
                players.get(1).resetBetChips();
                if(!"Allin".equals(players.get(0).getState()))
                    players.get(1).setState("Bet");
                setChanged();
                notifyObservers("playerChips");
                nextRound();
            }
            else if(players.get(1).getBetChips()>players.get(0).getLastBet()){
                potSize+=players.get(1).getBetChips();
                players.get(1).resetBetChips();
                players.get(1).setState("Bet");
                if("Bet".equals(players.get(0).getState()))
                    nrOfRounds--;
                players.get(0).setState("");
                setChanged();
                notifyObservers("playerChips");
                nextRound();
            }
        }
    }
    
    /**
     * handle clear bet event
     */
    public void clearBet(){
        if(isPlayer1){
            players.get(0).clearBetChips();
        }
        else{
            players.get(1).clearBetChips();
        }
        setChanged();
        notifyObservers("playerChips");
    }
    /**
     * handle next rount event
     */
    public void nextRound(){
        nrOfRounds++;
        System.out.println(nrOfRounds);
        if(nrOfRounds==2)
            dealFlop();
        if(nrOfRounds==4)
            dealRiver();
        if(nrOfRounds==6)
            dealTurn();
        if(nrOfRounds==8){
            roundWinner();
            if(isGameOver()){
                setChanged();
                notifyObservers("gameOver");
                nrOfRounds=0;
            }
            else{
                nrOfRounds=0;
                table.clear();
                setChanged();
                notifyObservers("nextRound");
            }
        }
        if("Allin".equals(players.get(1).getState()) && "Allin".equals(players.get(0).getState())){
            players.get(0).setState("");
            players.get(1).setState("");
            if(nrOfRounds<2){
                dealFlop();
                dealRiver();
                dealTurn();
            }
            else if(nrOfRounds<4){
                dealTurn();
                dealRiver();
            }
            else if(nrOfRounds<6 && nrOfRounds>=4){
                dealRiver();
            }
            if(!isPlayer1)
                setPlayersTurn();
            nrOfRounds=7;
            nextRound();
            }
        else if("Bet".equals(players.get(1).getState()) && "Bet".equals(players.get(0).getState())){
            players.get(0).setState("");
            players.get(1).setState("");
            players.get(0).clearLastBet();
            players.get(1).clearLastBet();
            if(isPlayer1)
                setPlayersTurn();
        }
        setPlayersTurn();
    }
    /**
     * check who won the round and notify view
     */
    public void roundWinner(){
        for (Player player : players) {
            player.setPoints();
            System.out.println(player.getPoints());
        }
        if(players.get(0).getPoints().ordinal()<players.get(1).getPoints().ordinal()){
            players.get(0).setChips(potSize);
            System.out.println("Player1 won");
            setChanged();
            notifyObservers("player1Won");
        }
        else if (players.get(0).getPoints().ordinal()>players.get(1).getPoints().ordinal()){
            players.get(1).setChips(potSize);
            System.out.println("Player2 won");
            setChanged();
            notifyObservers("player2Won");
        }
        else{
            if(players.get(0).getHighCard()>players.get(1).getHighCard()){
                players.get(0).setChips(potSize);
                System.out.println("Player1 won");
                setChanged();
                notifyObservers("player1Won");
            }
            else if (players.get(0).getHighCard()<players.get(1).getHighCard()){
                players.get(1).setChips(potSize);
                System.out.println("Player2 won");
                setChanged();
                notifyObservers("player2Won");
            }
            else{
                players.get(0).setChips(potSize/2);
                players.get(1).setChips(potSize/2);
                System.out.println("Its draw");
                setChanged();
                notifyObservers("draw");
            }
        }
        clearPotSize();
        setChanged();
        notifyObservers("playerChips");
        updateHighScore();
        
    }
    /**
     * 
     * @return true if game is over
     */
    public boolean isGameOver(){
        if(players.get(0).getChips()==0){
            System.out.println("GAME OVER PLAYER 2 won");
            return true;
        }
        if(players.get(1).getChips()==0){
            System.out.println("GAME OVER PLAYEER 1 WON");
            return true;
        }
        return false;
    }
    public void updateHighScore(){
        String tmp = players.get(0).getPoints().toString();
        highScore.updateHighScore(tmp);
        tmp = players.get(1).getPoints().toString();
        highScore.updateHighScore(tmp);
    }
    
    public HighScore getHighScore(){
        return highScore;
    }
    
    public void serialize() throws IOException {
        highScore.serialize();
    }
    
}
