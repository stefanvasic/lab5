/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author stefanvasic
 */
public class HighScore implements java.io.Serializable {
    private int nrOfRoyalFlush, nrOfStraight, nrOfStraightFlush, nrOfFlush,
            nrOfFullHouse, nrOfTwoPair, nrOfPair, nrOfThreeOfAKind, nrOfFourOfAKind;
    public HighScore(){
        
    }
    /**
     * update the highscore
     * @param rankEnum 
     */
    public void updateHighScore(String rankEnum ){
        switch(rankEnum){
            case"ROYAL_FLUSH": 
                nrOfRoyalFlush++; break;
            case"STRAIGHT_FLUSH":
                nrOfStraight++; break;
            case"FOUR_OF_A_KIND":
                nrOfFourOfAKind++; break;
            case"THREE_OF_A_KIND":
                nrOfThreeOfAKind++; break;
            case"FLUSH":
                nrOfFlush++; break;
            case"FULL_HOUSE":
                nrOfFullHouse++; break;
            case"TWO_PAIRS":
                nrOfTwoPair++; break;
            case"ONE_PAIR": 
                nrOfPair++;break;
            case"STRAIGHT":
                nrOfStraight++; break;
                
        }
    }
    
    @Override
    public String toString(){
        String info="Royal Flush: "+ nrOfRoyalFlush + "\nStraight Flush: " + nrOfStraightFlush + "\nFour Of A Kind: " + nrOfFourOfAKind + "\nFull House: " +
                nrOfFullHouse + "\nFlush: " + nrOfFlush + "\nStraight: " + nrOfStraight + "\nThree Of A Kind: " + nrOfThreeOfAKind + "\nTwo Pairs: " + 
                nrOfTwoPair + "\nOne Pair: " + nrOfPair;
        return info;
    }
    
    /**
     * serialize highscore
     * @throws IOException 
     */
    public void serialize()throws IOException{
        try
        {
         FileOutputStream fileOut =
         new FileOutputStream("highScore3.ser");
         ObjectOutputStream out = new ObjectOutputStream(fileOut);
         out.writeObject(this);
         out.close();
         fileOut.close();
        }catch(IOException i)
        {
            
        }    
    }
 
}
