/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kung
 */
public class HandRanking {
    public HandRanking(){
        
    }
    
       /**
        * 
     * @param player
        * @return true if the hand has a royal flush
        */ 
    public boolean checkRoyalFlush(Hand hand){ 
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>4){
            tmp.sort(Card.cardsByRankFirstThenSuit);
            for(int i=0;i<tmp.size()-4;i++){
                if(tmp.get(i).getRank().ordinal()==10){
                    if (tmp.get(i).getRank().ordinal()+1 == tmp.get(i+1).getRank().ordinal() && 
                    tmp.get(i+1).getRank().ordinal()+1 == tmp.get(i+2).getRank().ordinal() && 
                    tmp.get(i+2).getRank().ordinal()+1 == tmp.get(i+3).getRank().ordinal() && 
                    tmp.get(i+3).getRank().ordinal()+1 == tmp.get(i+4).getRank().ordinal()){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has a straight flush
     */
    public boolean checkStraightFlush(Hand hand){ 
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>4){
            tmp.sort(Card.cardsByRankFirstThenSuit);
            for(int i=0;i<tmp.size()-4;i++){
                if (tmp.get(i).getRank().ordinal()+1 == tmp.get(i+1).getRank().ordinal() && 
                tmp.get(i+1).getRank().ordinal()+1 == tmp.get(i+2).getRank().ordinal() && 
                tmp.get(i+2).getRank().ordinal()+1 == tmp.get(i+3).getRank().ordinal() && 
                tmp.get(i+3).getRank().ordinal()+1 == tmp.get(i+4).getRank().ordinal()){
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has a flush
     */
    public boolean checkFlush(Hand hand){   
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>4){
            tmp.sort(Card.cardsBySuit);
            for (int i=0;i<tmp.size()-4;i++){
                if (tmp.get(i).getSuit() == tmp.get(i+4).getSuit())
                    return true;
            }
        }   
        return false;
    }
    /**
     * 
     * @return true if hand has a straight
     */
    public boolean checkStraight(Hand hand){     
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>4){
            int counter=1;
            tmp.sort(Card.cardsByRank);
            for (int i=0;i<tmp.size();i++){
                for(int j=i+1;j<tmp.size()-1;j++){
                    if(tmp.get(i).getRank().ordinal() != tmp.get(j).getRank().ordinal()){
                        if(tmp.get(i).getRank().ordinal()+counter == tmp.get(j).getRank().ordinal()){
                            counter++;
                            if (counter==5)
                                return true;
                        }
                    }
                }
                counter=1;
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has four of a kind
     */
    public boolean checkFourOfAKind(Hand hand){   
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>3){
            tmp.sort(Card.cardsByRank);
            for (int i=0;i<tmp.size()-3;i++){
                if (tmp.get(i).getRank().ordinal() == tmp.get(i+3).getRank().ordinal())
                    return true;
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has three of king
     */
    public boolean checkThreeOfAKind(Hand hand){  
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>2){
            tmp.sort(Card.cardsByRank);
            for (int i=0;i<tmp.size()-2;i++){
                if (tmp.get(i).getRank().ordinal() == tmp.get(i+2).getRank().ordinal())
                    return true;
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has full house
     */
    public boolean checkFullHouse(Hand hand){   
        if(checkTwoPairs(hand)){
            if(checkThreeOfAKind(hand)){
                return true;
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has two pairs
     */
    public boolean checkTwoPairs(Hand hand){   
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>3){
            int nrOfPairs=0;
            tmp.sort(Card.cardsByRank);
            for (int i=0;i<tmp.size()-1;i++){
                if (tmp.get(i).getRank().ordinal() == tmp.get(i+1).getRank().ordinal()){
                    nrOfPairs++;
                    if(i<tmp.size()-2){
                        if(tmp.get(i).getRank().ordinal() == tmp.get(i+2).getRank().ordinal())
                            i++;
                    }
                    if(nrOfPairs==2)
                        return true;
                }      
            }
        }
        return false;
    }
    /**
     * 
     * @return true if hand has a pair
     */
    public boolean checkPair(Hand hand){      
        List<Card> tmp = new ArrayList<>(hand.getHand());
        if(tmp.size()>1){
            tmp.sort(Card.cardsByRank);
            for (int i=0;i<tmp.size()-1;i++){
                if (tmp.get(i).getRank().ordinal() == tmp.get(i+1).getRank().ordinal())
                    return true;
            }
        }
        return false;
    }
}
