package model;


import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
/**
 * Objects of this class represents cards in a deck (of cards). A card is
 * immutable, i.e. once created its rank or suit cannot be changed.
 */

public class Card {

    /**
     * @param rank 1 = Ace, 2 = 2, ...
     * @param suit 1 = spades, 2 = hearts, 3 = diamonds, 4 = clubs
     */
    private final Suit suit;
    private final Rank rank;
    
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }
    
    public int getValueOfCard(Card c){
        Rank r = c.rank;
        int value = r.ordinal();
        return value;
    }

    public boolean equals(Card other) {
        return this.rank == other.rank && this.suit == other.suit;
    }
    
    /**
     * Is a comperator that compare a cards suit to another cards suit and sorts it after suit.
     */
   
    public static Comparator<Card> cardsBySuit = new Comparator<Card> (){
        @Override
        public int compare(Card c1, Card c2) {
            return c1.getSuit().compareTo(c2.getSuit());
        }
    };
    
      public static Comparator<Card> cardsByRankAndSuit = new Comparator<Card>() {
          @Override
          public int compare(Card c1, Card c2) {
              if(c1.getRank().compareTo(c2.getRank()) == 0){
                  return c1.getSuit().compareTo(c2.getSuit());
              }
                  
              return c1.getRank().compareTo(c2.getRank());
          }
      };
      
  
   /**
     * Is a comperator that compare a cards rank to another cards rank and sorts it after rank.
     */
        public static Comparator<Card> cardsByRank = new Comparator<Card>(){
          @Override
          public int compare(Card c1, Card c2){
              return c1.getRank().compareTo(c2.getRank());
          }
      };
    
    
     /**
     * Is a comperator that sorts first by suit and then by rank
     */
        
    
    public static Comparator<Card> cardsBySuitAndRank = new Comparator<Card>(){
        @Override
        public int compare(Card c1, Card c2){
            if(c1.getSuit().compareTo(c2.getSuit()) == 0){
                return c1.getRank().compareTo(c2.getRank());
            }
            
            return c1.getSuit().compareTo(c2.getSuit());
        }
        
        
    };

        public static Comparator<Card> cardsByRankFirstThenSuit = new Comparator<Card>(){
        @Override
        public int compare(Card c1, Card c2){
            if(c1.getRank().compareTo(c2.getRank()) == 0){
                return c1.getSuit().compareTo(c2.getSuit());
            }
            
            return c1.getSuit().compareTo(c2.getSuit());
        }
        
        
    };

    @Override
    public String toString() {
        String info = rank  + " of " +  suit;
        return info;
    }

    public enum Suit {
        SPADES, HEARTS, CLUBS, DIAMONDS
    }
    
    public enum Rank {
        TWO (2), THREE (3), FOUR (4), FIVE (5), SIX (6), SEVEN (7), EIGHT (8), NINE (9), TEN (10),
        JACK (11), QUEEN (12), KING (13), ACE (14);
        
        private final int rank;
        Rank(int rank) { this.rank = rank; }
        public int getValue() { return rank;}
    }
}
