/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class AI extends Player{
    private double chanceOfWinning;
    private ChanceCalculator chanceCalculator;
    /**
     * creates a new AI
     */
    public AI(){
        super();
        super.setName("AI");
        chanceOfWinning=0.0;
        chanceCalculator = new ChanceCalculator();
    }
    /**
     * 
     * @return chance of winning 
     */
    public double getChanceOfWinning(){
        return chanceOfWinning;
    }
    /**
     * set chance of winning
     * @param chance 
     */
    public void setChanceOfWinning(){
        chanceOfWinning=chanceCalculator.calculateChanceOfHand(this);
    }
    
   
}
