/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
abstract public class Player extends Observable implements Serializable{
    private int chips;
    private Hand hand;
    private int betChips;
    private String name;
    private RankingEnum points;
    private String state;
    private int lastBet;
    private HandRanking handRanking;
    
    public Player(){
        hand = new Hand();
        chips=500;
        betChips=0;
        state="";
        lastBet=0;
        handRanking = new HandRanking();
    }
    /**
     * 
     * @return points, rankingEnum
     */
    public RankingEnum getPoints(){
        return points;
    }
    /**
     * 
     * @return chips
     */
    public int getChips(){
        return chips;
    }
    /**
     * 
     * @return betChips
     */
    public int getBetChips(){
        return betChips;
    }
    /**
     * 
     * @return hand
     */
    public Hand getHand(){
        return hand;
    }
    /**
     * 
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * 
     * @return state
     */
    public String getState(){
        return state;
    }
    /**
     * 
     * @return lastBet
     */
    public int getLastBet(){
        return lastBet;
    }
    /**
     * Sets a new name
     * @param name 
     */
    public void setName(String name){
        this.name=name;
    }
    /**
     * sets a new state
     * @param state 
     */
    public void setState(String state){
        this.state=state;
    }
    /**
     * sets players bet chips
     * @param bet 
     */
    public void setBetChips(int bet){
        if (bet<=chips){
            chips=chips-bet;
            betChips+=bet;
            lastBet+=bet;
        }
        setChanged();
        notifyObservers("playerChips");
    }
    /**
     * clear bet chips
     */
    public void clearBetChips(){
        chips+=betChips;
        betChips=0;
    }
    /**
     * clear last bet
     */
    public void clearLastBet(){
        lastBet=0;
    }
    /**
     * reset bet chips
     */
    public void resetBetChips(){
        betChips=0;
    }
    /**
     * sets the players chips to a new value
     * @param chips 
     */
     public void setChips(int chips){
        this.chips+=chips;
    }
    
    /**
     * 
     * @return the highest card
     */
    public int getHighCard(){
        List<Card> tmp = new ArrayList<>(getHand().getHand());
        tmp.sort(Card.cardsByRank);
        return tmp.get(getHand().getHand().size()-1).getRank().getValue();
    }
    /**
     * set the players points (rank enum)
     */
    public void setPoints(){
        if(handRanking.checkRoyalFlush(hand))
            points=RankingEnum.ROYAL_FLUSH;
        else if(handRanking.checkStraightFlush(hand))
            points=RankingEnum.STRAIGHT_FLUSH;
        else if(handRanking.checkFourOfAKind(hand))
            points=RankingEnum.FOUR_OF_A_KIND;
        else if(handRanking.checkFullHouse(hand))
            points=RankingEnum.FULL_HOUSE;
        else if(handRanking.checkFlush(hand))
            points=RankingEnum.FLUSH;
        else if(handRanking.checkStraight(hand))
            points=RankingEnum.STRAIGHT;
        else if(handRanking.checkThreeOfAKind(hand))
            points=RankingEnum.THREE_OF_A_KIND;
        else if(handRanking.checkTwoPairs(hand))
            points=RankingEnum.TWO_PAIRS;
        else if(handRanking.checkPair(hand))
            points=RankingEnum.ONE_PAIR;
        else   
            points=RankingEnum.HIGH_CARD;
    }

    
    @Override
    public String toString(){
        String info="";
        info += name + ": " + hand.toString();
        return info;
    }
    /**
     * Enums for ranking the hands
     */
    public enum RankingEnum{
        ROYAL_FLUSH, STRAIGHT_FLUSH, FOUR_OF_A_KIND, FULL_HOUSE, FLUSH,
        STRAIGHT, THREE_OF_A_KIND, TWO_PAIRS, ONE_PAIR, HIGH_CARD
    }
}
