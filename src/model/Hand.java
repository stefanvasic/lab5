package model;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
/**
 * Objects of this class represents a hand of cards. 
 */
public class Hand {
    private ArrayList<Card> hand;
    
    //Constructor
    public Hand(){
        hand = new ArrayList<>();
    }    
    
    public ArrayList<Card> getHand(){
        return hand;
    }
    /**
     * @return numer of cards in the hand
     */
    public int getNoOfCards(){
        int NoOfCards=hand.size();
        return NoOfCards;
    }
    
    /**
     * @param c the card added to the hand
     */
    public void addCard(Card c){
        hand.add(c);
    }
    
    /**
     * @param position of the hand
     * @return the card with the param position
     */
    public Card getCard(int position) throws NoSuchCardException{
        Card get=null;
        if(position>=hand.size() || position<0){
            throw new NoSuchCardException("Illegal index for card");
        }
        else{
             get = hand.get(position);
        }
        
        return get;
    }
    /**
     * @return sum is the summery of all the ranks in the hand
     */
    public int getValueOfHand(){
        int value=0;
        Card c;
        for (Card hand1 : hand) {
            c = hand1;
            Card.Rank r = c.getRank();
            value+=r.ordinal()+1;
        }
        return value;     
    }
    /*
    public void sortHandBySuit(){
        hand.sort(Card.cardsBySuit);
    }
    
    public void sortHandByRank(){
        hand.sort(Card.cardsByRank);
    }
    
    public void sortHandBySuitAndRank(){
        hand.sort(Card.cardsBySuitAndRank);
    }*/
    
    /**
     * @param c is the card to be removed
     * @return returns the removed card
     */
    public boolean removeCard(Card c){
        return hand.remove(c);
    }
    
    /**
     * @return info, string of the cards in the hand
     */
    @Override
    public String toString(){
        String info = "";
        for(int i=0;i<2;i++)
            info = info + hand.get(i).toString() + ", ";
        return info;
    }
}
