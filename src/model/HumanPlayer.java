/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class HumanPlayer extends Player{
    
    /**
     * creates a new human player
     * @param name 
     */
    public HumanPlayer(String name){
        super();
        super.setName(name);
    }
}
