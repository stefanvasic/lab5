/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;


/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class GameButtons {
    private Image flipButtonImage, chip5Image, chip10Image, chip25Image,
            betButtonImage, checkButtonImage, foldButtonImage, allInButtonImage,
            clearBetButtonImage, nextRoundImage;
    private ImageView flipButtonView, chip5View, chip10View,
            chip25View, betButtonView, checkButtonView, foldButtonView, allInButtonView
            , clearBetButtonView, nextRoundView;
    private Button flipButton, flipButton2, chip5Button, chip10Button, chip25Button,
            betButton, checkButton, foldButton, allInButton, clearBetButton,
            nextRoundButton;
    private HBox chipBox,eventButtonsBox ;
    private Canvas canvas;
    
    public GameButtons(Canvas canvas){
        this.canvas = canvas;
        createFlipButtons(true);
        createEventButtons();
        createChipsButtons();
    }
    
    /**
     * create event buttons
     */
    void createEventButtons(){
        betButtonImage= new Image(this.getClass().getResource("/resources/betButton.gif").toString());
        checkButtonImage= new Image(this.getClass().getResource("/resources/checkButton.gif").toString());
        foldButtonImage= new Image(this.getClass().getResource("/resources/foldButton.gif").toString());
        allInButtonImage= new Image(this.getClass().getResource("/resources/allInButton.gif").toString());
        nextRoundImage= new Image(this.getClass().getResource("/resources/nextRoundButton.jpg").toString());
        betButtonView = new ImageView();
        checkButtonView = new ImageView();
        foldButtonView = new ImageView();
        allInButtonView = new ImageView();
        nextRoundView = new ImageView();

        betButtonView.setImage(betButtonImage);
        checkButtonView.setImage(checkButtonImage);
        foldButtonView.setImage(foldButtonImage);
        allInButtonView.setImage(allInButtonImage);
        nextRoundView.setImage(nextRoundImage);
        
        betButtonView.setFitHeight(55);
        betButtonView.setFitWidth(70);
        checkButtonView.setFitHeight(55);
        checkButtonView.setFitWidth(70);
        foldButtonView.setFitHeight(55);
        foldButtonView.setFitWidth(70);
        allInButtonView.setFitHeight(55);
        allInButtonView.setFitWidth(70);
        
        betButton = new Button("", betButtonView);
        checkButton = new Button("", checkButtonView);
        foldButton = new Button("", foldButtonView);
        allInButton = new Button("", allInButtonView);
        nextRoundButton = new Button("", nextRoundView);
        
        nextRoundButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        betButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        checkButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        foldButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        allInButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        eventButtonsBox = new HBox();
        eventButtonsBox.getChildren().addAll( betButton, checkButton, foldButton,allInButton);
        eventButtonsBox.setSpacing(80);
        
        AnchorPane.setLeftAnchor(eventButtonsBox, canvas.getWidth()*0.50);
        AnchorPane.setBottomAnchor(eventButtonsBox, canvas.getHeight()*0.20);
        
        AnchorPane.setLeftAnchor(nextRoundButton, canvas.getWidth()*0.2);
        AnchorPane.setBottomAnchor(nextRoundButton, canvas.getHeight()*0.60);
    }
    /**
     * create flipbuttons 
     * @param playerVsPlayer if true create two flip buttons
     */
    void createFlipButtons(boolean playerVsPlayer){
        flipButtonImage = new Image(this.getClass().getResourceAsStream("/resources/flipButton.gif"));
        flipButtonView = new ImageView();
        flipButtonView.setImage(flipButtonImage);
        flipButton = new Button("", flipButtonView);
        flipButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        
        AnchorPane.setBottomAnchor(flipButton, canvas.getHeight()*0.4);
        AnchorPane.setLeftAnchor(flipButton, canvas.getWidth()*0.25);
        
        if(playerVsPlayer == true){
            ImageView flipButtonView2 = new ImageView();
            flipButtonView2.setImage(flipButtonImage);
            flipButton2 = new Button("", flipButtonView2);
            flipButton2.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
            AnchorPane.setBottomAnchor(flipButton2,canvas.getHeight()*0.4);
            AnchorPane.setLeftAnchor(flipButton2, canvas.getWidth()*0.75);
        }
    }
    /**
     * create chips buttons
     */
    private void createChipsButtons(){
        chip5Image = new Image(this.getClass().getResource("/resources/5-chips.gif").toString());
        chip10Image = new Image(this.getClass().getResource("/resources/10-Chip.gif").toString());
        chip25Image = new Image(this.getClass().getResource("/resources/25-chip.gif").toString());
        clearBetButtonImage = new Image(this.getClass().getResource("/resources/clearBetButton.gif").toString());
        chip5View = new ImageView();
        chip10View = new ImageView();
        chip25View = new ImageView();
        clearBetButtonView = new ImageView();
        chip5View.setImage(chip5Image);
        chip10View.setImage(chip10Image);
        chip25View.setImage(chip25Image);
        
        clearBetButtonView.setFitHeight(55);
        clearBetButtonView.setFitWidth(70);
        clearBetButtonView.setImage(clearBetButtonImage);
        
        chip5Button = new Button("", chip5View);
        chip10Button = new Button("", chip10View);
        chip25Button = new Button("", chip25View);
        clearBetButton = new Button("", clearBetButtonView);
        
         clearBetButton.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        chip5Button.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        chip10Button.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        chip25Button.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 3px; " +
                "-fx-min-height: 3px; " +
                "-fx-max-width: 3px; " +
                "-fx-max-height: 3px;"
        );
        
        chipBox = new HBox();
        chipBox.getChildren().addAll(clearBetButton, chip5Button,chip10Button, chip25Button);
        chipBox.setSpacing(70);
        AnchorPane.setLeftAnchor(chipBox, canvas.getWidth()*0.20);
        AnchorPane.setBottomAnchor(chipBox, canvas.getHeight()*0.20);
        
        
    }
    /**
     * 
     * @return chip5 button
     */
    public Button getChip5Button(){
        return chip5Button;
    }
    /**
     * 
     * @return chip10 button
     */
    public Button getChip10Button(){
        return chip10Button;
    }
    /**
     * 
     * @return chip25 button
     */
    public Button getChip25Button(){
        return chip25Button;
    }
    /**
     * 
     * @return flip button1
     */
    public Button getFlipButton1(){
        return flipButton;
    }
    /**
     * 
     * @return flip button2
     */
    public Button getFlipButton2(){
        return flipButton2;
    }
    /**
     * 
     * @return the box that contains the chips buttons
     */
    public HBox getChipBox(){
        return chipBox;
    }
    /**
     * 
     * @return the box that contains the event buttons
     */
    public HBox getEventButtonsBox(){
        return eventButtonsBox;
    }
    /**
     * 
     * @return bet button
    */
    public Button getBetButton(){
        return betButton;
    }
    /**
     * 
     * @return check button
    */
    public Button getCheckButton(){
        return checkButton;
    }
    /**
     * 
     * @return fold button
    */
    public Button getFoldButton(){
        return foldButton;
    }
    /**
     * 
     * @return all in button
    */
    public Button getAllInButton(){
        return allInButton;
    }
    /**
     * 
     * @return clear bet button
    */
    public Button getClearBetButton(){
        return clearBetButton;
    }
    /**
     * 
     * @return next round button
    */
    public Button getNextRoundButton(){
        return nextRoundButton;
    }
}
