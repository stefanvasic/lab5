/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.Observable;
import java.util.Observer;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.PokerModel;

/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class PlayerLabels implements Observer{
    private TableView tableView;
    private PokerModel model;
    private Label player1Name, player2Name, player1Chips, player2Chips,
            aiActionLabel, tablePotSize, roundWinner, playersTurn;
    
    public PlayerLabels(TableView tableView, PokerModel model){
        this.tableView=tableView;
        this.model=model;
        createPlayerNameLabels();
        createPlayerChipsLabels();
        createRoundWinnerLabel();
        createPlayersTurnLabel();
    }
    /**
     * 
     * @param o the observable object to call notify
     * @param arg the arguments that is passed
     */
    @Override
    public void update(Observable o, Object arg) {
         if(null != arg.toString())switch (arg.toString()) {
            case "playerNames":
                model = (PokerModel) o;
                player1Name.setText(model.getPlayers().get(0).getName());
                player1Chips.setText("Chips: " + model.getPlayers().get(0).getChips());
                if(model.getPlayers().size()>1){
                    player2Name.setText(model.getPlayers().get(1).getName());
                    player2Chips.setText("Chips: " + model.getPlayers().get(1).getChips());
                    if("AI".equals(model.getPlayers().get(1).getName()))
                        createAIActionLabel();
                }  break;
            case "playerChips":
                model = (PokerModel) o;
                player1Chips.setText("Chips: " + model.getPlayers().get(0).getChips());
                player2Chips.setText("Chips: " + model.getPlayers().get(1).getChips());
                tablePotSize.setText("Pot size: " + model.getPotSize());
                break;
            case "AIaction": aiActionLabel.setText("Ai action: " + model.getPlayers().get(1).getState());
        }
         if(null != arg.toString())switch (arg.toString()) {
            case "player1Won":
                model = (PokerModel) o;
                roundWinner.setText("PLAYER 1 WINS WITH " + model.getPlayers().get(0).getPoints().toString().replaceAll("_", " "));
                break;
            case "player2Won":
                model = (PokerModel) o;
                roundWinner.setText("PLAYER 2 WINS WITH " + model.getPlayers().get(1).getPoints().toString().replaceAll("_", " "));
                break;
            case "draw":
                model = (PokerModel) o;
                roundWinner.setText("IT'S A DRAW YOU SPLIT THE POT" );
                break;
            case "updateTurn":
                playersTurn.setText(model.getPlayersTurn() + "  Your Turn");
                break;
        }
    }
    /**
     * create player name labels
     */
    public void createPlayerNameLabels(){
        player1Name = new Label("Player 1");
        player1Name.setFont(new Font("Arial", 14));
        player1Name.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(player1Name, tableView.getCanvas().getHeight()*0.53);
        AnchorPane.setLeftAnchor(player1Name, tableView.getCanvas().getWidth()*0.28);
        tableView.getAnchorPane().getChildren().add(player1Name);
        
        player2Name = new Label("Player2");
        player2Name.setFont(new Font("Arial", 14));
        player2Name.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(player2Name, tableView.getCanvas().getHeight()*0.53);
        AnchorPane.setLeftAnchor(player2Name, tableView.getCanvas().getWidth()*0.59);
        tableView.getAnchorPane().getChildren().add(player2Name);
    }
    /**
     * create round winner label
     */
    public void createRoundWinnerLabel(){
        roundWinner = new Label("");
        roundWinner.setFont(new Font("Arial", 20));
        roundWinner.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(roundWinner, tableView.getCanvas().getHeight()*0.05);
        AnchorPane.setLeftAnchor(roundWinner, tableView.getCanvas().getWidth()*0.35);
        tableView.getAnchorPane().getChildren().add(roundWinner);
    }
    /**
     * create player chips labels
     */
    public void createPlayerChipsLabels(){
        player1Chips = new Label("Chips: 0");
        player1Chips.setFont(new Font("Arial", 14));
        player1Chips.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(player1Chips, tableView.getCanvas().getHeight()*0.53);
        AnchorPane.setLeftAnchor(player1Chips, tableView.getCanvas().getWidth()*0.36);
        tableView.getAnchorPane().getChildren().add(player1Chips);
        
        player2Chips = new Label("Chips: 0");
        player2Chips.setFont(new Font("Arial", 14));
        player2Chips.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(player2Chips, tableView.getCanvas().getHeight()*0.53);
        AnchorPane.setLeftAnchor(player2Chips, tableView.getCanvas().getWidth()*0.67);
        tableView.getAnchorPane().getChildren().add(player2Chips);
        
        tablePotSize = new Label("Pot size: 0");
        tablePotSize.setFont(new Font("Arial", 16));
        tablePotSize.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(tablePotSize, tableView.getCanvas().getHeight()*0.48);
        AnchorPane.setLeftAnchor(tablePotSize, tableView.getCanvas().getWidth()*0.45);
        tableView.getAnchorPane().getChildren().add(tablePotSize);
    }
    /**
     * create player turn label
     */
    public void createPlayersTurnLabel(){
        playersTurn = new Label("Player 1 Your Turn");
        playersTurn.setFont(new Font("Arial", 16));
        playersTurn.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(playersTurn, tableView.getCanvas().getHeight()*0.10);
        AnchorPane.setLeftAnchor(playersTurn, tableView.getCanvas().getWidth()*0.42);
        tableView.getAnchorPane().getChildren().add(playersTurn);
    }
    /**
     * create ai action label
     */
    public void createAIActionLabel(){
        aiActionLabel = new Label("AI");
        aiActionLabel.setFont(new Font("Arial", 16));
        aiActionLabel.setTextFill(Color.WHITE);
        AnchorPane.setTopAnchor(aiActionLabel, tableView.getCanvas().getHeight()*0.50);
        AnchorPane.setLeftAnchor(aiActionLabel, tableView.getCanvas().getWidth()*0.70);
        tableView.getAnchorPane().getChildren().add(aiActionLabel);
    }
}
