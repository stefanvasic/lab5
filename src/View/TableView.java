package View;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.PokerModel;
import Controller.PokerController;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.util.Duration;
import model.Card;


/**
 *
 * @author Stefan Vasic And Manjinder Singh
 */
public class TableView  extends BorderPane implements Observer{
    public static Observer test;
    private PokerModel model;
    private GameButtons gameButtons;
    private PokerController controller;
    private Menu fileMenu;
    private MenuItem exitGame, highscore, startGame;
    private MenuBar menuBar;
    private Canvas canvas;
    private AnchorPane anchorPane;
    private StackPane deckBox;
    private Image image, flopImage, turnImage, riverImage,
                        turnCardImage, deckImage ;
    private ImageView flopView, flopView2, flopView3, turnView, riverView, 
             deckImageView[],turnCardView,  player1Card1ImageView,
            player1Card2ImageView, player2Card1ImageView, player2Card2ImageView;
    private HBox turnRiverBox, flopBox,  turnRiverCardBox,cardFlopBox;
    private Stage stage, stageHighscore;
    private TextField p1Name, p2Name;
    private GridPane popUp;
    private BorderPane highscorePopUp;
    private GraphicsContext gc;
    private Point2D playerCards[], deckImagePoint;
    private Boolean isPlayer1Flipped=false, isPlayer2Flipped=false;
    private Button nrOfPlayers;
    private int windowHeight, windowWidth;
    
    /**
     * create a new table view
     * this is the main graphics
     * @param model
     * @param windowWidth
     * @param windowHeight 
     */
    public TableView(PokerModel model, int windowWidth, int windowHeight){
         this.model = model;
         this.controller = new PokerController(this, model);
         System.out.println(windowHeight);
         System.out.println(windowWidth);
         this.windowWidth=windowWidth;
         this.windowHeight=windowHeight;
         initView();
    }
    
    /**
     * initialize the view
     */
    public void initView (){
       
        canvas = new Canvas(windowWidth, windowHeight);
        gc = canvas.getGraphicsContext2D();

        anchorPane = new AnchorPane();
        
        anchorPane.getChildren().add(canvas);
        
        this.setCenter(anchorPane);
        createMenuBar();
        loadBackground();
        loadCardBox();
        loadDeckBox();
       
        addMenuBarHandlers(controller);
        PlayerLabels playerLabels = new PlayerLabels(this, model);
        model.addObserver(playerLabels);
    }
    /**
     * 
     * @param o the object to call notify
     * @param arg the arguments that is passes
     */
    @Override
    public void update(Observable o, Object arg) {
        if("showcard".equals(arg.toString())){
            model = (PokerModel) o ;
            switch(model.getTable().size()){
                case 3: showFlop(model.getTable().get(0), model.getTable().get(1), model.getTable().get(2)); break;
                case 4: showTurn(model.getTable().get(3)); break;
                case 5: showRiver(model.getTable().get(4)); break;
            }
        }
        if("nextRound".equals(arg.toString())){
            if(isPlayer1Flipped==false){
                flipPlayer1Cards();
            }
            if(isPlayer2Flipped==false){
                flipPlayer2Cards();
            }
            anchorPane.getChildren().add(gameButtons.getNextRoundButton());
            anchorPane.getChildren().remove(gameButtons.getEventButtonsBox());
            anchorPane.getChildren().remove(gameButtons.getChipBox());
            gameButtons.getNextRoundButton().setOnAction(new EventHandler <ActionEvent>(){
                
                @Override
                public void handle(ActionEvent event) {
                     removeCardsFromTable();
                     loadDeckBox();
                     animateDealCards();
                     anchorPane.getChildren().remove(gameButtons.getNextRoundButton());
                     controller.handleResetGame();
                     anchorPane.getChildren().add(gameButtons.getChipBox());
                     anchorPane.getChildren().add(gameButtons.getEventButtonsBox());
                }
          
           });
       }
        
       if(null != arg.toString())switch (arg.toString()) {
            case "removeCardsOnTable":
                removeCardsFromTable();
                loadDeckBox();
                animateDealCards();
                controller.handleResetGame();
                break;
            case "dealCardsToPlayers":
                model = (PokerModel) o ;
                dealCardsToPlayer1(model.getPlayers().get(0).getHand().getCard(0), 
                        model.getPlayers().get(0).getHand().getCard(1));
                dealCardsToPlayer2(model.getPlayers().get(1).getHand().getCard(0), 
                        model.getPlayers().get(1).getHand().getCard(1));
                break;
        }
        if("gameOver".equals(arg.toString())){
             stage.showAndWait();
        }
    }
    
    /**
     * create the menu bar
     */
    public void createMenuBar(){
        fileMenu = new Menu("Game");
        startGame = new MenuItem("Start New Game");
        highscore = new MenuItem("Highscore");
        exitGame = new MenuItem("Exit");
        fileMenu.getItems().addAll(startGame, highscore, exitGame);
        menuBar= new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        this.setTop(menuBar);
    }
    
    /**
     * load the background
     */
    public void loadBackground(){
        image = new Image(this.getClass().getResource("/resources/PokerTable2-test.jpg").toString());
        deckImage = new Image(this.getClass().getResourceAsStream("/resources/b2fv.png"));
        Image playerCardHolderImage = new Image(this.getClass().getResourceAsStream("/resources/playerCardHolder.jpg"));

        playerCards = new Point2D[4];
        playerCards[0] = new Point2D(windowWidth*0.3,windowHeight*0.57);
        playerCards[1] = new Point2D(windowWidth*0.35,windowHeight*0.6);
        playerCards[2] = new Point2D(windowWidth*0.6,windowHeight*0.6);
        playerCards[3] = new Point2D(windowWidth*0.65,windowHeight*0.57);
        
        HBox cardBox = new HBox();
        AnchorPane.setBottomAnchor(cardBox, windowHeight*0.45);
        AnchorPane.setLeftAnchor(cardBox, windowWidth*0.30);
        
        deckImagePoint = new Point2D(windowWidth*0.65, windowHeight*0.2);
        
        gc.drawImage(image, 0, 0, windowWidth, windowHeight);
        gc.drawImage(deckImage, windowWidth*0.65, windowHeight*0.20, 45, 60);
        gc.drawImage(playerCardHolderImage, playerCards[0].getX(), playerCards[0].getY(), 45, 60);
        gc.drawImage(playerCardHolderImage, playerCards[1].getX(), playerCards[1].getY(), 45, 60);
        gc.drawImage(playerCardHolderImage, playerCards[2].getX(), playerCards[2].getY(), 45, 60);
        gc.drawImage(playerCardHolderImage, playerCards[3].getX(), playerCards[3].getY(), 45, 60);
        
        anchorPane.getChildren().add(cardBox);
    }
    /**
     * pop up when starting a new game
     */
    public void startGamePopUp(){
        
        EventHandler nrOfPlayersHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                controller.handleNrOfPlayers(p1Name.getText(),p2Name.getText());
                stage.hide();
                controller.handleResetGame();
            }
        };

        popUp = new GridPane();
        Label label =new Label("Leave player2 name empty to play against AI ");
        Label player1 = new Label("Player 1 name: ");
        p1Name = new TextField();
        popUp.add(player1, 0, 1);
        popUp.add(p1Name, 1, 1);
        Label player2 = new Label("Player 2 name: ");
        p2Name = new TextField();
        popUp.add(player2, 0, 2);
        popUp.add(p2Name, 1, 2);
        popUp.add(label, 0, 0);
        nrOfPlayers = new Button("OK");
        popUp.add(nrOfPlayers, 1, 3);
        popUp.setVgap(20);
        popUp.setHgap(20);
        
        nrOfPlayers.setOnAction(nrOfPlayersHandler);
        
        Scene scene2 = new Scene(popUp,500,300);
        
        stage = new Stage();
        stage.setScene(scene2);
        stage.initModality(Modality.APPLICATION_MODAL);
        
        stage.showAndWait();
    }
    
    public void highscorePopUp(){
        EventHandler highscoreHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                stageHighscore.hide();
            }
        };
        highscorePopUp = new BorderPane();
        TextArea highscoreText = new TextArea(model.getHighScore().toString());
        highscoreText.setEditable(false);
        highscoreText.setStyle("-fx-font-size: 16px;");
        highscorePopUp.setCenter(highscoreText);
        Label highscoreLabel = new Label("Highscore");
        highscorePopUp.setTop(highscoreLabel);
        Button okButton = new Button("Ok");
        highscorePopUp.setBottom(okButton);
        okButton.setOnAction(highscoreHandler);
        
        Scene scene3 = new Scene(highscorePopUp,500,300);
        
        stageHighscore = new Stage();
        stageHighscore.setScene(scene3);
        stageHighscore.initModality(Modality.APPLICATION_MODAL);
        
        stageHighscore.showAndWait();
        
    }
    /**
     * load the deck image box
     */
    void loadDeckBox(){
        deckImageView = new ImageView[4];
        deckBox = new StackPane();
        
        for(int i=0; i<deckImageView.length;i++){
            deckImageView[i] = new ImageView();
            deckImageView[i].setFitHeight(60);
            deckImageView[i].setFitWidth(45);
            deckImageView[i].setImage(deckImage);
            deckBox.getChildren().add(deckImageView[i]);
        }
        AnchorPane.setLeftAnchor(deckBox, windowWidth*0.65);
        AnchorPane.setTopAnchor(deckBox, windowHeight*0.20);
        anchorPane.getChildren().add(deckBox);
    }
    
    /**
     * load the buttons
     * @param playerVsPlayer if false dont load flipbutton for AI
     */
    public void loadButtons(Boolean playerVsPlayer){
        gameButtons = new GameButtons(canvas);
        anchorPane.getChildren().add(gameButtons.getEventButtonsBox());
        anchorPane.getChildren().add(gameButtons.getChipBox());
        
        EventHandler flipButtonsHandler = new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if(event.getSource()==gameButtons.getFlipButton1()){
                    controller.handleFlipButton1Event();
                }
                else if(event.getSource()==gameButtons.getFlipButton2())
                    controller.handleFLipButton2Event();
            }
        };
        
        if(playerVsPlayer){
            anchorPane.getChildren().add(gameButtons.getFlipButton2());
            gameButtons.getFlipButton2().setOnAction(flipButtonsHandler);
        }
        anchorPane.getChildren().add(gameButtons.getFlipButton1());
        gameButtons.getFlipButton1().setOnAction(flipButtonsHandler);
    }
    /**
     * 
     * @return gamebuttons
     */
    public GameButtons getGameButtons(){
        return gameButtons;
    }
    /**
     * load the card holders box to the table
     */
    private void loadCardBox(){
        flopImage= new Image(this.getClass().getResource("/resources/Flop.jpg").toString());
        turnImage= new Image(this.getClass().getResourceAsStream("/resources/Turn.jpg"));
        riverImage= new Image(this.getClass().getResource("/resources/River.jpg").toString());
    
        flopView = new ImageView();
        turnView = new ImageView();
        riverView = new ImageView();
        flopView2 = new ImageView();
        flopView3 = new ImageView();
        
        flopView.setImage(flopImage);
        flopView2.setImage(flopImage);
        flopView3.setImage(flopImage);
        turnView.setImage(turnImage);
        riverView.setImage(riverImage);
        
        flopBox = new HBox();
        flopBox.setSpacing(30);
        flopBox.getChildren().addAll(flopView, flopView2, flopView3);
        
        turnRiverBox = new HBox();
        turnRiverBox.setSpacing(30);
        turnRiverBox.getChildren().addAll(turnView, riverView);
        
        AnchorPane.setBottomAnchor(flopBox, windowHeight*0.55);
        AnchorPane.setLeftAnchor(flopBox, windowWidth*0.30);
        AnchorPane.setBottomAnchor(turnRiverBox, windowHeight*0.55);
        AnchorPane.setLeftAnchor(turnRiverBox, windowWidth*0.55);
        
        anchorPane.getChildren().add(flopBox);
        anchorPane.getChildren().add(turnRiverBox);
    }
    /**
     * show flop on table
     * @param c1 first card
     * @param c2 second card
     * @param c3 third card
     */
    public void showFlop(Card c1, Card c2, Card c3){
        String card1 = c1.toString().replaceAll("\\s", "");
        String card2 = c2.toString().replaceAll("\\s", "");
        String card3 = c3.toString().replaceAll("\\s", "");
        
        Image card1Image= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card1 + ".png"));
        Image card2Image= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card2 + ".png"));
        Image card3Image= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card3 + ".png"));
    
        ImageView card1View = new ImageView();
        ImageView card2View = new ImageView();
        ImageView card3View = new ImageView();
        
        card1View.setImage(card1Image);
        card2View.setImage(card2Image);
        card3View.setImage(card3Image);
        
        card1View.setFitHeight(60);
        card1View.setFitWidth(45);
        card2View.setFitHeight(60);
        card2View.setFitWidth(45);
        card3View.setFitHeight(60);
        card3View.setFitWidth(45);
        cardFlopBox = new HBox();
        cardFlopBox.setSpacing(30);
        cardFlopBox.getChildren().addAll(card1View, card2View, card3View);

        AnchorPane.setBottomAnchor(cardFlopBox, windowHeight*0.55);
        AnchorPane.setLeftAnchor(cardFlopBox, windowWidth*0.30);
        
        anchorPane.getChildren().add(cardFlopBox);
        System.out.println("adddiiiiiing");
    }
    /**
     * show turn on table
     * @param c1 
     */
    public void showTurn(Card c1){
        String turnCard = c1.toString().replaceAll("\\s", "");
        turnCardImage= new Image(this.getClass().getResourceAsStream("/resources/cards/" + turnCard + ".png"));
        
        turnCardView = new ImageView();
        
        turnCardView.setImage(turnCardImage);
        
        turnCardView.setFitHeight(60);
        turnCardView.setFitWidth(45);
        
        turnRiverCardBox = new HBox();
        turnRiverCardBox.setSpacing(30);
        turnRiverCardBox.getChildren().add(turnCardView);
        
        AnchorPane.setBottomAnchor(turnRiverCardBox, windowHeight*0.55);
        AnchorPane.setLeftAnchor(turnRiverCardBox, windowWidth*0.55);
        
        anchorPane.getChildren().add(turnRiverCardBox);
    }
    /**
     * display river on table
     * @param c1 
     */
    public void showRiver(Card c1){
        String riverCard = c1.toString().replaceAll("\\s", "");
        Image riverCardImage= new Image(this.getClass().getResourceAsStream("/resources/cards/" + riverCard + ".png"));
        
        ImageView riverCardView = new ImageView();
        
        riverCardView.setImage(riverCardImage);
        
        riverCardView.setFitHeight(60);
        riverCardView.setFitWidth(45);
   
        turnRiverCardBox.getChildren().add(riverCardView);
    }
    /**
     * remove all cards from the table
     */
    public void removeCardsFromTable(){
        anchorPane.getChildren().remove(turnRiverCardBox);
        anchorPane.getChildren().remove(cardFlopBox);
        anchorPane.getChildren().remove(deckBox);
        if(isPlayer1Flipped==true){
            anchorPane.getChildren().remove(player1Card1ImageView);
            anchorPane.getChildren().remove(player1Card2ImageView);
            isPlayer1Flipped=false;
            System.out.println("teeeeeest");
        }
         if(isPlayer2Flipped==true){
            anchorPane.getChildren().remove(player2Card1ImageView);
            anchorPane.getChildren().remove(player2Card2ImageView);
            isPlayer2Flipped=false;
        }
    }
    /**
     * flip player 1's cards
     */
    public void flipPlayer1Cards(){
        if(isPlayer1Flipped==false){
            anchorPane.getChildren().add(player1Card1ImageView);
            anchorPane.getChildren().add(player1Card2ImageView);
            isPlayer1Flipped=true;
        }
        else{

            anchorPane.getChildren().remove(player1Card1ImageView);
            anchorPane.getChildren().remove(player1Card2ImageView);

            isPlayer1Flipped=false;
        }
    }
    /**
     * flip player 2's cards
     */
    public void flipPlayer2Cards(){
        if(isPlayer2Flipped==false){
            
            anchorPane.getChildren().add(player2Card1ImageView);
            anchorPane.getChildren().add(player2Card2ImageView);
            isPlayer2Flipped=true;
        }
        else{
            anchorPane.getChildren().remove(player2Card1ImageView);
            anchorPane.getChildren().remove(player2Card2ImageView);
            isPlayer2Flipped=false;
        }
    }
    /**
     * deal 2 cards to player 1
     * @param c1
     * @param c2 
     */
    void dealCardsToPlayer1(Card c1, Card c2){
        
        String card1Name = c1.toString().replaceAll("\\s", "");
        Image card1= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card1Name + ".png"));
        player1Card1ImageView = new ImageView();
        player1Card1ImageView.setImage(card1);
        player1Card1ImageView.setFitHeight(60);
        player1Card1ImageView.setFitWidth(45);
        AnchorPane.setTopAnchor(player1Card1ImageView, playerCards[0].getY());
        AnchorPane.setLeftAnchor(player1Card1ImageView, playerCards[0].getX());
        
        String card2Name = c2.toString().replaceAll("\\s", "");
        Image card2= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card2Name + ".png"));
        player1Card2ImageView = new ImageView();
        player1Card2ImageView.setImage(card2);
        player1Card2ImageView.setFitHeight(60);
        player1Card2ImageView.setFitWidth(45);
        AnchorPane.setTopAnchor(player1Card2ImageView, playerCards[1].getY());
        AnchorPane.setLeftAnchor(player1Card2ImageView, playerCards[1].getX());
    }
    /**
     * deal 2 cards to player 2
     * @param c1
     * @param c2 
     */
    void dealCardsToPlayer2(Card c1, Card c2){
        String card1Name = c1.toString().replaceAll("\\s", "");
        Image card1= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card1Name + ".png"));
        player2Card1ImageView = new ImageView();
        player2Card1ImageView.setImage(card1);
        player2Card1ImageView.setFitHeight(60);
        player2Card1ImageView.setFitWidth(45);
        AnchorPane.setTopAnchor(player2Card1ImageView, playerCards[2].getY());
        AnchorPane.setLeftAnchor(player2Card1ImageView, playerCards[2].getX());
        
        String card2Name = c2.toString().replaceAll("\\s", "");
        Image card2= new Image(this.getClass().getResourceAsStream("/resources/cards/" + card2Name + ".png"));
        player2Card2ImageView = new ImageView();
        player2Card2ImageView.setImage(card2);
        player2Card2ImageView.setFitHeight(60);
        player2Card2ImageView.setFitWidth(45);
        AnchorPane.setTopAnchor(player2Card2ImageView, playerCards[3].getY());
        AnchorPane.setLeftAnchor(player2Card2ImageView, playerCards[3].getX());
    }
    /**
     * animate dealing cards from deck to player
     */
    public void animateDealCards()  {
        TranslateTransition  translate[];
        translate = new TranslateTransition[4];
        
        for(int i=0; i<translate.length; i++){
            translate[i] = new TranslateTransition(Duration.seconds(4), deckImageView[i]);
            translate[i].setFromX( 0);
            translate[i].setFromY(0);
        
            translate[i].setToX( playerCards[i].getX()-deckImagePoint.getX() );
            translate[i].setToY(playerCards[i].getY()-deckImagePoint.getY());
            translate[i].setCycleCount( 1 );
            translate[i].play();
        }
    }
    /**
     * add event handlers
     * @param controller 
     */
    public void addEventHandlers(PokerController controller){
        EventHandler eventButtonsHandler = new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if(event.getSource()==gameButtons.getFoldButton()){
                    controller.handleFoldButtonEvent();
                }
                else if(event.getSource()==gameButtons.getBetButton()){
                    controller.handleBetButtonEvent();
                }
                else if(event.getSource()==gameButtons.getCheckButton()){
                    controller.handleCheckButtonEvent();
                }
                else if(event.getSource()==gameButtons.getAllInButton()){
                    controller.handleAllInEvent();
                }
                else if(event.getSource()==gameButtons.getChip5Button())
                {
                    controller.handleChip5ButtonEvent();
                }
                else if(event.getSource()==gameButtons.getChip10Button())
                {
                    controller.handleChip10ButtonEvent();
                }
                else if(event.getSource()==gameButtons.getChip25Button())
                {
                    controller.handleChip25ButtonEvent();
                }
                else if(event.getSource()==gameButtons.getClearBetButton()){
                    controller.handleClearBetEvent();
                }
            }
        };
        gameButtons.getChip10Button().setOnAction(eventButtonsHandler);
        gameButtons.getChip5Button().setOnAction(eventButtonsHandler);
        gameButtons.getChip25Button().setOnAction(eventButtonsHandler);
        gameButtons.getClearBetButton().setOnAction(eventButtonsHandler);
        gameButtons.getBetButton().setOnAction(eventButtonsHandler);
        gameButtons.getAllInButton().setOnAction(eventButtonsHandler);
        gameButtons.getFoldButton().setOnAction(eventButtonsHandler);
        gameButtons.getCheckButton().setOnAction(eventButtonsHandler);
    }
    
    /**
     * add menubar handlers
     * @param controller
     */
    public void addMenuBarHandlers(PokerController controller){
        EventHandler startGameHandler = new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                controller.handleStartGame();
                controller.handleLoadButtons();
            }
            
        };
        startGame.setOnAction(startGameHandler); 
        
        EventHandler exitHandler = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    controller.handleExitEvent();
                } catch (IOException ex) {
                    Logger.getLogger(TableView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        exitGame.setOnAction(exitHandler);
        
        EventHandler highscoreHandler = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                controller.handleHighscore();
            }
                
        };
        highscore.setOnAction(highscoreHandler);
    }
    /**
     * 
     * @return anchorpane 
     */
    public AnchorPane getAnchorPane(){
        return this.anchorPane;
    }
    /**
     * 
     * @return canvas 
     */
    public Canvas getCanvas(){
        return canvas;
    }
}
